import { defineStore } from 'pinia'
import { IPokemon } from 'src/interfaces/IUser.interface'
import { ref } from 'vue'
import { HttpClient } from 'src/HttpClient/http'

const route = 'pokemon'
const { http } = new HttpClient()

export const usePokemonStore = defineStore('pokemonStore', () => {
  const pokemon = ref<IPokemon | null>(null)
  const pokemones = ref<IPokemon[]>([])

  const getPokemon = async (id: string) => {
    const { data } = await http.get(`${route}/${id}`)
    const especies = await http.get(`pokemon-species/${id}`)
    pokemon.value = {
      ...data,
      species: especies.data.genera[7].genus,
    }

  }

  const getAllPokemones = async (query: any) => {
    let data = null
    if (query) {
      data = await http.get(`${query}`)
    } else {
      data = await http.get(`${route}?offset=0&limit=20`)
    }
    const results = data.data.results
      results.next = data.data.next
      results.previous = data.data.previous
      results.count = data.data.count
      for (const i of results) {
        const detail = await http.get(`${i.url}`)
        i.detail = detail.data
        i.img = detail.data.sprites.other.home.front_default
      }

      pokemones.value = results
  }

  return {
    getAllPokemones,
    getPokemon,
    pokemon,
    pokemones
  } as const

})

