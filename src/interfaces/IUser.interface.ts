
export interface IPokemon {
  order: number;
  name: string;
  moves: [];
  abilities: [];
  base_experience: number;
  height: number;
  weight: number;
  ability: [];
  species: [];
  types: [];
  stats: [];
}

export interface AllPokemon {
  count: number;
  results: [];
}


